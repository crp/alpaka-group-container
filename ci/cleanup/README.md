# Attention

The script has a bug and deletes all tags of a image, if only on tag matched.

# preparation

The clean up scripts needs some preparation in the GitLab repository. Please do the following instructions:

1. Go to your fork of the project on GitLab.com
2. Create a [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token) (`Settings->Access Tokens`) with role `Maintainer` and the permission `api`, `read_registry` and `write_registry`.
3. Set the token as [variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) with name `PRIVATE_JOB_TOKEN` in the CI (`Settings->CI/CD`). Don't forget to set Mask variable flag for security reasons.

# clean_up_after_job.py

The script is executed on a function branch after each successful CI run to delete all created Docker images.

For testing purpose create a project access token and setup the following environment variables, before you execute the script.

* **CI_SERVER_URL**: URL of the GitLab instance. Normally https://codebase.helmholtz.cloud
* **PRIVATE_JOB_TOKEN**: Create a temporary project access. Go to `Settings->Access` Tokens 
* **CI_PROJECT_ID**: ID of the main project (4742) or you fork. Go to `Settings->General`
* **TAG**: Tag of the docker images to delete.
