import gitlab
import os

if __name__ == "__main__":
    for env_var in ["CI_SERVER_URL", "PRIVATE_JOB_TOKEN", "CI_PROJECT_ID", "TAG"]:
        if not env_var in os.environ:
            print(f"{env_var} environment variable is not defined")
            exit(1)

    print(f"CI_SERVER_URL: {os.environ['CI_SERVER_URL']}")
    print(f"CI_PROJECT_ID: {os.environ['CI_PROJECT_ID']}")
    print(f"TAG: {os.environ['TAG']}")

    gl = gitlab.Gitlab(
        url=os.environ["CI_SERVER_URL"],
        private_token=os.environ["PRIVATE_JOB_TOKEN"],
    )

    gl.auth()
    print("login successful")

    project = gl.projects.get(os.environ["CI_PROJECT_ID"])

    print(f"project name: {project.attributes['name']}")
    print(f"project url:  {project.attributes['web_url']}")

    repositories = project.repositories.list(get_all=True)

    if not isinstance(repositories, list):
        repositories = [repositories]

    tag_name = os.environ["TAG"]

    delete_ids = []

    print("deleted images: ")
    for repo in repositories:
        for tag in repo.tags.list():
            if tag_name == tag.attributes["name"]:
                print(f"{repo.attributes['name']}:{tag.attributes['name']}")
                delete_ids.append(repo.get_id())
                break

    for repo_id in delete_ids:
        project.repositories.delete(repo_id)
