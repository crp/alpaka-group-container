"""Script to generate build matrix for alpaka group container."""

import argparse, json, yaml
from typing import Dict, List, Tuple, Optional

# constants
STAGE_BASE = "base"
STAGE_GCC = "gcc"
STAGE_GPU = "gpu"
STAGE_PIC = "pic"
STAGE_NAMES = [STAGE_BASE, STAGE_GCC, STAGE_GPU, STAGE_PIC]

KNOWN_UBUNTU_VERSIONS = ["22.04", "24.04"]


class BuildTree:
    def __init__(self, ubuntu_version: str, branch_name: str):
        """The BuildTree represent the different alpaka-group-container as tree, because the
        different images bases on each other. The order of image types is:
        base |-> gcc -> gpu
             |-> gpu

        Each image can be also extended with a PIConGPU layer.

        The constructor creates a new base image. Other image types are created by the member
        functions add_gcc(), add_gpu(), add_pic() because they are related to other images.

        Each node of the BuildTree stores it parent and children nodes if exists.

        Args:
            ubuntu_version (str): Ubuntu version of the base image.
            branch_name (str): Git branch name. Some parts of the jobs are depending of if the
            branch is the master or not

        Raises:
            RuntimeError: If non supported Ubuntu version is chosen.
        """
        if not ubuntu_version in KNOWN_UBUNTU_VERSIONS:
            raise RuntimeError(
                f"Unknown Ubuntu Version: {ubuntu_version}\n"
                f"Known versions: {KNOWN_UBUNTU_VERSIONS}"
            )

        # reference to parent node
        # None if object is base node
        self.parent: Optional[BuildTree] = None
        self.type: str = STAGE_BASE
        self.gpu: str = ""
        self.children: List[BuildTree] = []
        self.ubuntu_version: str = ubuntu_version
        self.branch_name: str = branch_name

    def add_gcc(self) -> "BuildTree":
        """Create a image with different installed GCC versions basing on the parent image.
        The new node is added to the children list of the node, where the member function is called.

        Raises:
            RuntimeError: If function is applied on a node, which does not have the type base.

        Returns:
            BuildTree: Reference to the created node.
        """
        if self.type != STAGE_BASE:
            raise RuntimeError("gcc can only added to BuildTree of type base.")

        node = BuildTree(self.ubuntu_version, self.branch_name)
        node.type = STAGE_GCC
        node.parent = self

        self.children.append(node)
        return node

    def add_gpu(self, gpu: str) -> "BuildTree":
        """Create a image with an installed GPU SDK basing on the parent image.
        The new node is added to the children list of the node, where the member function is called.

        Args:
            gpu (str): Name of GPU SDK. At the moment cuda and rocm supported.

        Raises:
            RuntimeError: If function is applied on a node, which does not have the type base or gcc.
            RuntimeError: If gpu is unknown.

        Returns:
            BuildTree: Reference to the created node.
        """
        if not (self.type == STAGE_BASE or self.type == STAGE_GCC):
            raise RuntimeError("gpu can only added to BuildTree of type base or gcc.")

        if not (gpu.startswith("cuda") or gpu.startswith("rocm")):
            raise RuntimeError(f"Unknown gpu type: {gpu}")

        node = BuildTree(self.ubuntu_version, self.branch_name)
        node.type = STAGE_GPU
        node.parent = self
        node.gpu = gpu

        self.children.append(node)
        return node

    def add_pic(self) -> "BuildTree":
        """Create a image with installed PIConGPU requirements basing on the parent image.
        The new node is added to the children list of the node, where the member function is called.

        Returns:
            BuildTree: Reference to the created node.
        """
        node = BuildTree(self.ubuntu_version, self.branch_name)
        node.type = STAGE_PIC
        node.parent = self

        self.children.append(node)
        return node

    def get_image_name(self) -> str:
        """Returns the image name of this node.

        Returns:
            str: image name of the node
        """
        gcc = False
        gpu = ""
        pic = False

        current_node = self

        while current_node != None:
            if current_node.type == STAGE_GCC:
                gcc = True
            elif current_node.type == STAGE_GPU:
                gpu = current_node.gpu
            elif current_node.type == STAGE_PIC:
                pic = True

            current_node = current_node.parent

        image_name = get_image_name(self.ubuntu_version, gcc, gpu, pic)

        return image_name

    def get_number_of_parents(self) -> int:
        """Return number of parents of this node.

        Returns:
            int: number of parents
        """
        current_node = self
        number_of_parents = -1

        while current_node != None:
            current_node = current_node.parent
            number_of_parents += 1

        return number_of_parents

    def dump(self):
        """Print image name of this node and all child nodes. Use indentation to visualize
        relationships.
        """
        image_name = self.get_image_name()
        number_of_parents = self.get_number_of_parents()

        name_size_length = 40

        print(
            number_of_parents * name_size_length * " "
            + image_name.ljust(name_size_length, " "),
        )

        for child in self.children:
            child.dump()

    def get_image_name_list(self) -> List[str]:
        """Get list image names containing the image name of this and all children nodes.

        Returns:
            List[str]: list of image names
        """
        image_names = [self.get_image_name()]

        for child in self.children:
            image_names += child.get_image_name_list()

        return image_names

    def generate_job_yaml(self, output_yaml: Dict[str, Dict]):
        """Generate job yaml for this and all children nodes.

        Args:
            output_yaml (Dict[str, Dict]): The job yaml, where all job descriptions are added. Needs
            to be initialized entries of the type Dict[str, Dict]. Each key is a entry STAGE_NAMES
            and each value is a empty list.

        Raises:
            RuntimeError: If the member variable gpu of a node is unknown
            RuntimeError: If a node has an unknown type.
        """
        depending_image = "" if self.parent == None else self.parent.get_image_name()
        job_dict = get_base_job(
            self.branch_name, self.get_image_name(), depending_image
        )

        if self.type == STAGE_BASE:
            args = " ".join(
                [
                    f"--ubuntu={self.ubuntu_version}",
                    "--cmake",
                    "--boost",
                    "--catch2",
                    "--clangppa",
                ]
            )
            job_dict["script"] = [f'ARGS="{args}"']
        elif self.type == STAGE_GCC:
            args = " ".join(
                [
                    f"--ubuntu={self.ubuntu_version}",
                    '--baseimage="${BASE_IMAGE}"',
                    "--gcc",
                ]
            )
            job_dict["script"] = [
                'BASE_IMAGE="${CI_REGISTRY_IMAGE}/' + depending_image + ':${TAG}"',
                f'ARGS="{args}"',
            ]
        elif self.type == STAGE_GPU:
            if self.gpu.startswith("cuda"):
                gpu_arg = "--cuda=" + str(float(self.gpu[len("cuda") :]) / 10.0)
            elif self.gpu.startswith("rocm"):
                gpu_arg = "--rocm=" + self.gpu[len("rocm") :]
            else:
                raise RuntimeError(f"Unknown gpu type: {self.gpu}")

            args = " ".join(
                [
                    f"--ubuntu={self.ubuntu_version}",
                    '--baseimage="${BASE_IMAGE}"',
                    gpu_arg,
                ]
            )
            job_dict["script"] = [
                'BASE_IMAGE="${CI_REGISTRY_IMAGE}/' + depending_image + ':${TAG}"',
                f'ARGS="{args}"',
            ]
        elif self.type == STAGE_PIC:
            args = " ".join(
                [
                    f"--ubuntu={self.ubuntu_version}",
                    '--baseimage="${BASE_IMAGE}"',
                    "--picongpu",
                ]
            )
            job_dict["script"] = [
                'BASE_IMAGE="${CI_REGISTRY_IMAGE}/' + depending_image + ':${TAG}"',
                f'ARGS="{args}"',
            ]
        else:
            raise RuntimeError(f"BuildTree type: {self.type}")

        append_script_section_post_process(job_dict["script"], self.branch_name)

        image_name = self.get_image_name()
        output_yaml[self.type].update({image_name: job_dict})

        for child in self.children:
            child.generate_job_yaml(output_yaml)


def get_image_name(ubuntu_version: str, gcc: bool, gpu: str, pic: bool) -> str:
    """Creates the image name depending on the input parameter.

    Args:
        ubuntu_version (str): Used Ubuntu version.
        gcc (bool): Different GCC versions are installed.
        gpu (str): A specific GPU SDK is installed.
        pic (bool): The PIConGPU requirements are installed.

    Raises:
        RuntimeError: If the Ubuntu version is unknown.

    Returns:
        str: image name
    """
    if not ubuntu_version in KNOWN_UBUNTU_VERSIONS:
        raise RuntimeError(
            f"Unknown Ubuntu Version: {ubuntu_version}\n"
            f"Known versions: {KNOWN_UBUNTU_VERSIONS}"
        )

    image_name = "alpaka-ci-ubuntu" + ubuntu_version

    if gpu:
        image_name += "-" + gpu

    if gcc:
        image_name += "-gcc"

    if pic:
        image_name += "-pic"

    return image_name


def get_base_job(
    branch_name: str,
    image_name: str,
    depending_image: str,
) -> Dict[str, Dict]:
    """Return part of the container build job, which is fully independent of the node type.

    Args:
        branch_name (str): Name of the git branch.
        image_name (str): Name of the base image
        depending_image (str): Name of the image, which this job is depending on. Can be empty if
        there is no image.

    Returns:
        Dict[str, Dict]: Dict with job description for a GitLab CI job.
    """
    job_dict = {}
    job_dict["image"] = "docker:latest"
    job_dict["services"] = ["docker:dind"]
    job_dict["before_script"] = [
        "docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY",
        "apk update && apk add python3 py3-pip",
        "python3 -m venv /venv && source /venv/bin/activate",
        "pip3 install -r generator/requirements.txt",
        "VERSION=$(head -n 1 version.txt)",
        'IMAGE_NAME="${CI_REGISTRY_IMAGE}/' + image_name + '"',
        (
            "TAG=${VERSION}"
            if branch_name == "master"
            else "TAG=$(echo ${CI_COMMIT_BRANCH} | awk '{print tolower($0)}')-${VERSION};"
        ),
        'echo "VERSION -> $VERSION"',
        'echo "IMAGE_NAME -> $IMAGE_NAME"',
        'echo "TAG -> $TAG"',
    ]

    job_dict["tags"] = ["docker"]

    if depending_image:
        job_dict["needs"] = [depending_image]

    return job_dict


def append_script_section_post_process(script: List, branch_name: str):
    """Add all instruction to the script section of the jobs, which are independent of the node
    type.

    Args:
        script (List): Content of the script section of a job
        branch_name (str): Git branch name.
    """
    script += [
        'echo "ARGS -> ${ARGS}"',
        "python3 recipe.py $ARGS --version $VERSION > Dockerfile",
        'docker build -t "$IMAGE_NAME:$TAG" .',
        'docker push "$IMAGE_NAME:$TAG"',
    ]
    if branch_name == "master":
        script += [
            'docker tag "$IMAGE_NAME:$TAG" "$IMAGE_NAME:latest"',
            'docker push "$IMAGE_NAME:latest"',
        ]


def create_build_tree(config: Dict, branch_name: str) -> List[BuildTree]:
    """Create a list of BuildTree's from the a config. Each BuildTree has a base image (ubuntu
    version) as root node.

    Args:
        config (Dict): Configuration read from images.json
        branch_name (str): Git branch name

    Returns:
        List[BuildTree]: List of BuildTree's
    """
    base_nodes = []
    # base stage
    for ubuntu_version in config.keys():
        base_node = BuildTree(ubuntu_version, branch_name)
        base_node.add_pic()
        base_nodes.append(base_node)
        # gcc stage
        for gcc in config[ubuntu_version]:
            if gcc == "gcc":
                gcc_node = base_node.add_gcc()
                gcc_node.add_pic()
                # gpu stage
                for gpu in config[ubuntu_version][gcc]:
                    gpu_node = gcc_node.add_gpu(gpu)
                    gpu_node.add_pic()
            else:
                # gpu stage
                for gpu in config[ubuntu_version][gcc]:
                    gpu_node = base_node.add_gpu(gpu)
                    gpu_node.add_pic()

    return base_nodes


def write_job_yaml(path: str, output_yaml: Dict[str, Dict]):
    """Write GitLab jobs to a yaml file. Adds the different stages.

    Args:
        path (str): Path of the yaml file.
        output_yaml (Dict[str, Dict]): Dict containing all GitLab CI jobs.
    """
    with open(path, "w", encoding="utf-8") as f:
        # write stages
        yaml.dump({"stages": STAGE_NAMES}, f)
        f.write("\n")

        for stage in STAGE_NAMES:
            f.write(f"###############\n# {stage}\n###############\n")

            for job_name, job_body in output_yaml[stage].items():
                job_body["stage"] = stage
                yaml.dump({job_name: job_body}, f)
                f.write("\n")
            f.write("\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="GenerateBuildJobs",
        description="Script to generate build matrix for alpaka group container.",
    )
    parser.add_argument(
        "configpath",
        type=str,
        help="Path to the image.json.",
    )
    parser.add_argument(
        "-o",
        type=str,
        default="jobs.yml",
        help="Path of the generated GitLab CI job.yml",
    )
    parser.add_argument(
        "--branch",
        type=str,
        default="master",
        help="Name of the git branch. Some parts of the jobs are different, if the branch is not "
        "the master branch.",
    )

    parser.add_argument(
        "--dump-image-names",
        action="store_true",
        help="Dump the names of all generated images with indentation to show relationships.",
    )

    parser.add_argument(
        "--get-image-names",
        action="store_true",
        help="Return names of all generated images as JSON List.",
    )

    args = parser.parse_args()

    with open(args.configpath, "r", encoding="utf-8") as f:
        config = json.load(f)

    base_nodes = create_build_tree(config, args.branch)

    if args.dump_image_names:
        for base_node in base_nodes:
            base_node.dump()
        exit(0)

    if args.get_image_names:
        image_names: List[str] = []
        for base_node in base_nodes:
            image_names += base_node.get_image_name_list()
        print(image_names)
        exit(0)

    # the jobs in the generated jobs.yml are sorted by stages
    output_yaml = {}
    for stage in STAGE_NAMES:
        # create entry for each stage, because following up functions adds entries
        output_yaml[stage] = {}

    for base_node in base_nodes:
        base_node.generate_job_yaml(output_yaml)

    write_job_yaml(args.o, output_yaml)
