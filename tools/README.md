# AGC Manager

The `agc-manager` is a small to manage the installed software in the container. The tool has 2 main function.

1. Asking whether a software or a specific version of the software is installed.
2. Retrieve the base installation path of the software, if it exists. 

# Usage

The tool is located inside the container. Therefore, you need to start the container to access it. Run `agc-manager --help` to get all functions. The following documentation shows you how to use the most important functions in your CI scripts.

## Check for Installed Software

With `agc-manager -e cmake` you can check if one or more `cmake` versions are installed. The agc-manager uses the return value to give the information (0 for not installed and 1 for installed).

```bash
# use the agc-manager to check if cmake is installed, otherwise install it
if ! agc-manager -e cmake; then
  echo "install cmake"
  # ...
fi
```

You can also ask for a specific version, such as `agc-mamager -e cmake@3.20.5`. The `agc-manager` supports intelligent version matching, i.e. if you specify only the major version `cmake@3`, you will get all cmake 3 versions. If you specify major and minor version (`agc-mamager -e cmake@3.20`), you will get all cmake 3.20 versions, regardless of patch level and so on.

## Get the Base Path

The base path of a software can be used to set include and linker paths or to add an executable to `PATH`. The function `agc-manager -b cmake` prints the base path to `stdout`. If more than one version matches, all base paths are printed line by line. If a software is installed but there is no base path, e.g. the host `gcc`, where headers, libraries and executables do not have the same base path, an empty line is printed.

```bash
# select only one match
# in practice it is better to specify one version to get only one result
cmake_path=$(agc-manager -b cmake | head -n 1 )
PATH=${cmake_path}/bin:$PATH
```
