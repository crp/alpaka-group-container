# Copyright 2022 Simeon Ehrig
#
# ISC Software License

import json, os, sys, argparse
from typing import Tuple

software_file = "/etc/installed_software.json"


def parseArgs() -> argparse.Namespace:
    """Parse the input arguments.

    :returns: argparse.Namespace object
    :rtype: argparse.Namespace

    """
    parser = argparse.ArgumentParser(
        description="Give information about installed software in the container",
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument(
        "-b",
        "--base-path",
        type=str,
        help=f"Prints the base path of the installed software."
        "The version specified like in the option --exists"
        "Prints the base paths of all matching version. If no base path is specified, print empty line.",
    )

    parser.add_argument(
        "-d", "--dump", action="store_true", help=f"Dumps the {software_file}"
    )

    parser.add_argument(
        "-e",
        "--exists",
        type=str,
        help=f"""
Check if software is installed. Returns 0 if it installed and 1 if not.
The software can specified by name and name with version.
    
    * -exists cmake
    * -exists cmake@3
    * -exists cmake@3.20
    * -exists cmake@3.20.5
    """,
    )

    return parser.parse_args()


def parse_software_string(sw_sting: str) -> Tuple[str, str]:
    """Parse software name version string and extract name and
    version. The string can have the shape of:

    * name
    * name@version

    If more, than one @ is used, the behavior is undefined.

    :param sw_sting: software name with optional version
    :type name: str
    :returns: Return tuple of (name, version).
    If there is no version defined, the version string is empty.
    :rtype: Tuple[str, str]
    """
    if "@" in sw_sting:
        exists_tmp = sw_sting.split("@")
        return (exists_tmp[0], exists_tmp[1])
    else:
        return (sw_sting, "")


def starts_with_version(searched_version: str, given_version: str) -> bool:
    """Compares two version strings. The given_version string must provide
    a version string with more or the same information as the
    searched_version string. The function uses str.starts_width for the
    comparison and handles non-numeric characters to separate the
    different version levels.

    :param searched_version: The string to check if it is equal to or a
    subversion of given_version.
    :type name: str
    :param searched_version: The target string.
    :type name: str
    :returns: Return True if matches.
    :rtype: bool
    """
    return given_version.startswith(searched_version) and (
        # catch the case that version 3.2 is searched and 3.20 is installed
        len(given_version) == len(searched_version)
        # check if the char after 3.2 is not a digit in the installed version
        or not given_version[len(searched_version)].isdigit()
    )


def main():
    if not os.path.exists(software_file):
        print(f"{software_file} does not exist!", file=sys.stderr)

    with open(software_file) as f:
        software_json = json.load(f)

    args = parseArgs()

    # dump json file to output
    if args.dump:
        print(json.dumps(software_json, indent=2))
        exit(0)

    # remove version from key name
    sw_names = []
    for s in software_json:
        sw_names.append(s.split("@")[0])

    if args.exists:
        sw_name, sw_version = parse_software_string(args.exists)

        if not sw_version:
            # needs to negate that True becomes return value 0
            exit(not (sw_name in sw_names))
        else:
            if not sw_name in sw_names:
                exit(1)
            for installation in software_json:
                if installation.startswith(sw_name):
                    if starts_with_version(
                        searched_version=sw_version,
                        # the string after the @ is the version
                        given_version=installation.split("@")[1],
                    ):
                        exit(0)
            exit(1)

    if args.base_path:
        sw_name, sw_version = parse_software_string(args.base_path)

        if not sw_name in sw_names:
            print("")
            exit(1)

        for installation in software_json:
            if installation.startswith(sw_name):
                if not sw_version or starts_with_version(
                    searched_version=sw_version,
                    # the string after the @ is the version
                    given_version=installation.split("@")[1],
                ):
                    print(software_json[installation]["base_path"])


if __name__ == "__main__":
    main()
