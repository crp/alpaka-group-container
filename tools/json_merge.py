# Copyright 2020 Simeon Ehrig
#
# ISC Software License

import argparse, json


def main():
    parser = argparse.ArgumentParser(
        description="Merge two json files and print the result to stdout. "
        "If both json files contains the same key, the new one overwrites the old one.",
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument(
        "-o", type=str, required=True, help="Path to the old json file."
    )

    parser.add_argument(
        "-n", type=str, required=True, help="Path to the new json file."
    )

    args = parser.parse_args()

    with open(args.o) as f:
        old_json = json.load(f)

    with open(args.n) as f:
        new_json = json.load(f)

    old_json.update(new_json)
    print(json.dumps(old_json, indent=2))


if __name__ == "__main__":
    main()
