# alpaka-group-container

This repository contains container recipes for [alpaka](https://github.com/alpaka-group/alpaka) and alpaka based projects, like [cupla](https://github.com/alpaka-group/cupla) and [vikunja](https://github.com/alpaka-group/vikunja) for CI testing.

# install

```bash
pip install -r generator/requirements.txt
```

# usage

```bash
python recipe.py --ubuntu=20.04 > Dockerfile
# It is required that you build the container from the project root. Otherwise, a Docker copy command will fail.
docker build -t alpaka-ci:latest .
```

To see all commands, run `python recipe.py --help`. It is also possible to build multistage container:

```bash
python recipe.py --ubuntu=20.04 > Dockerfile
# It is required that you build the container from the project root. Otherwise, a Docker copy command will fail.
docker build -t alpaka-ci:base .
# Use alpaka-ci:base as base image and install cuda on top
python recipe.py --ubuntu=20.04 --baseimage=alpaka-ci:base --cuda=11.6 > Dockerfile
docker build -t alpaka-ci:cuda .
```

# structure

* `recipe.py` is a frontend for the terminal and handles terminal arguments to generate different container recipes.
* `generator/` contains the actual code for recipe generation. The scripts contains different layers that can be stacked to a complete recipe.
* `tools/agc-manager.py` is a tool which is copied into the container. The tool allows to ask which software is installed in the container. For more information, please read the [README.md](tools/README.md) in the `tools` folder.
* `ci/` contains the scripts for the CI pipeline. The CI automatically creates and deploys the various container images.

# The CI pipeline

A CI pipeline is implemented which automatically build images and push then to the container registry: https://codebase.helmholtz.cloud/crp/alpaka-group-container/container_registry

The available container images are configured in the image.json file. The structure is designed as a tree, which means that there are dependencies between the different images. The tree is represented as a nested dictionary in the `image.json`.

* The top-level dictionary defines the Ubuntu version of all images. This image contains all the basic software requirements, such as CMake, Boost, Catch2 and more.
* The second level decides whether different GCC versions are installed or not. If the value is `none`, only the system GCC is installed.
* The third level decides which GPU SDK is installed. At the moment `CUDA` and `ROCm` are available.

Each entry in the `image.json` creates an image that can be downloaded from the registry. This also applies to nodes in the middle of the dependency tree. For each image a `PIConGPU` version is also built automatically. 

# Contributing

**Important:** If you fork the project and want to do a merge request, you need to increase the CI timeout in your forked project to 3h. You can find the instructions for doing this [here](https://docs.gitlab.com/ee/ci/pipelines/settings.html#timeout). If you did it correctly, the CI jobs will show a timeout of 3h:

![CI Timeout](doc/images/ci_timeout.png)

**Clean up the registry:** The different stages of the CI require that some Docker images are stored as intermediate results. When the CI pipeline runs completely successfully, these temporary images are automatically deleted.
