# Copyright 2023 Simeon Ehrig
#
# ISC Software License

"""Generate recipes for CI jobs of alpaka based projects.
"""

import argparse

import hpccm
import generator as gn


def parse_args() -> argparse.Namespace:
    """Parse the input arguments.

    :returns: argparse.Namespace object
    :rtype: argparse.Namespace

    """
    parser = argparse.ArgumentParser(
        description="Generator of Dockerfiles for alpaka and alpaka based projects.",
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument(
        "--ubuntu",
        type=float,
        required=True,
        help="Select Ubuntu base image.",
        choices=gn.ubuntu_versions.keys(),
    )

    parser.add_argument(
        "--baseimage",
        type=str,
        help="Specific your own base image path. "
        "The base image needs to base on Ubuntu 20.04. "
        "If a custom base image is specified, does not apply base_layer().",
    )

    parser.add_argument(
        "--cmake",
        type=str,
        nargs="*",
        choices=gn.sw_versions["cmake"],
        help="Install specific cmake versions.\n"
        "If flag is set without argument, install all cmake versions.",
    )

    parser.add_argument(
        "--boost",
        type=str,
        nargs="*",
        choices=gn.sw_versions["boost"],
        help="Install specific boost versions.\n"
        "If flag is set, without argument, install all boost versions.",
    )

    parser.add_argument(
        "--catch2",
        type=str,
        nargs="*",
        choices=gn.sw_versions["catch2"],
        help="Install specific Catch2 versions.\n"
        "If flag is set, without argument, install all Catch2 versions.",
    )

    parser.add_argument(
        "--gcc",
        type=str,
        nargs="*",
        choices=gn.sw_versions["gcc"],
        help="Install additional gcc compiler beside the system compiler.\n"
        "If only --gcc is passed without assignment, all available gcc will be installed.\n",
    )

    parser.add_argument("--clangppa", action="store_true", help="Add clang/llvm ppa's")

    parser.add_argument(
        "--cuda",
        type=float,
        help="Select CUDA SDK version.",
        choices=gn.cuda_versions.keys(),
    )

    parser.add_argument(
        "--rocm",
        type=float,
        help="Select ROCm SDK version.",
        choices=gn.rocm_versions.keys(),
    )

    parser.add_argument(
        "--picongpu",
        action="store_true",
        help="Install all requirements for PIConGPU.",
    )

    parser.add_argument(
        "--version",
        type=str,
        help="Set a version which is store as AGC_CONTAINER_VERSION environment variable.",
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    stage = hpccm.Stage()

    if not args.baseimage:
        stage.baseimage(
            image=gn.ubuntu_versions[args.ubuntu]["image"],
            _distro=gn.ubuntu_versions[args.ubuntu]["distro"],
        )
        for command in gn.base_layer(args.ubuntu):
            stage += command
    else:
        stage.baseimage(
            image=args.baseimage,
            _distro=gn.ubuntu_versions[args.ubuntu]["distro"],
        )

    # if the argument is not use, the value is None
    # if the argument is used without assignment (--cmake) the value is an empty list
    if args.cmake is not None:
        if len(args.cmake) == 0:
            cmake_versions = gn.sw_versions["cmake"]
        else:
            cmake_versions = args.cmake

        for command in gn.cmake_layer(cmake_versions, args.ubuntu):
            stage += command

    if args.catch2 is not None:
        if len(args.catch2) == 0:
            catch2_versions = gn.sw_versions["catch2"]
        else:
            catch2_versions = args.catch2

        for command in gn.catch2_layer(catch2_versions, args.ubuntu):
            stage += command

    if args.boost is not None:
        if len(args.boost) == 0:
            boost_versions = gn.sw_versions["boost"]
        else:
            boost_versions = args.boost

        for command in gn.boost_layer(boost_versions, args.ubuntu):
            stage += command

    if args.gcc is not None:
        if len(args.gcc) == 0:
            gcc_versions = gn.sw_versions["gcc"]
        else:
            gcc_versions = args.gcc

        for command in gn.gcc_layer(gcc_versions, args.ubuntu):
            stage += command

    if args.clangppa:
        for command in gn.clang_ppa_layer(args.ubuntu):
            stage += command

    if args.cuda:
        for command in gn.cuda_layer(args.cuda, args.ubuntu):
            stage += command

    if args.rocm:
        for command in gn.rocm_layer(args.rocm, args.ubuntu):
            stage += command

    if args.picongpu:
        for command in gn.picongpu_layer():
            stage += command

    if args.version:
        stage += hpccm.primitives.environment(
            variables={
                "AGC_CONTAINER_VERSION": args.version,
            }
        )

    for command in gn.final_layer():
        stage += command

    print(stage)
