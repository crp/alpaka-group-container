# Copyright 2022 Simeon Ehrig
#
# ISC Software License

"""Add clang/llvm ppa's.

available versions: 6.0, 7, 8, 9, 10, 11, 12, 13, 14, 15
"""

from typing import List
from generator.versions import ubuntu_versions

from hpccm.primitives import comment, shell


def specific_version_string(name: str, llvm_version: str) -> List[str]:
    """Create ppa string for specific llvm version.

    :param name: Ubuntu name (e.g. focal).
    :type name: str
    :param llvm_version: Version of clang/llvm.
    :type llvm_version: str
    :returns: List of bash commands.
    :rtype: List[str]

    """
    return [
        f'echo "deb http://apt.llvm.org/{name}/ '
        f'llvm-toolchain-{name}-{llvm_version} main"'
        " >> /etc/apt/sources.list.d/llvm.list",
        f'echo "deb-src http://apt.llvm.org/{name}/ '
        f'llvm-toolchain-{name}-{llvm_version} main"'
        " >> /etc/apt/sources.list.d/llvm.list",
        'echo "" >> /etc/apt/sources.list',
    ]


def clang_ppa_layer(ubuntu_version: float) -> List:
    """Add clang/llvm ppa's.

    :param ubuntu_version: Ubuntu version of used base container (eg. 20.04).
    :type ubuntu_version: str
    :returns: List of hpccm objects that can be attached to the stage
    :rtype: List

    """
    commands = [
        comment("#######################"),
        comment("#### clang ppa layer ###"),
        comment("#######################"),
    ]

    ubuntu_code_name = ubuntu_versions[ubuntu_version]["image"].split(":")[1]

    ppas = [
        "wget -qO- https://apt.llvm.org/llvm-snapshot.gpg.key | "
        "tee /etc/apt/trusted.gpg.d/apt.llvm.org.asc",
        f'echo "deb http://apt.llvm.org/{ubuntu_code_name}/ llvm-toolchain-{ubuntu_code_name} main "'
        " >> /etc/apt/sources.list.d/llvm.list",
        f'echo "deb-src http://apt.llvm.org/{ubuntu_code_name}/ llvm-toolchain-{ubuntu_code_name} main"'
        " >> /etc/apt/sources.list.d/llvm.list",
        'echo "" >> /etc/apt/sources.list',
    ]

    ppas += specific_version_string(ubuntu_code_name, "18")
    ppas += specific_version_string(ubuntu_code_name, "19")

    commands.append(shell(commands=ppas))

    return commands
