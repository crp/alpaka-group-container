# Copyright 2023 Simeon Ehrig
#
# ISC Software License

"""Install gcc.
"""

from typing import List

from hpccm.primitives import comment
from hpccm.building_blocks.gnu import gnu
from generator.util import add_installed_software
from generator.versions import get_versions


def gcc_layer(versions: List[str], ubuntu_version: float) -> List:
    """Install gcc via apt.

    :param versions: List of the version to be installed.
    :type versions: List[str]
    :param ubuntu_version: Ubuntu version of used base container (eg. 20.04).
    :type ubuntu_version: float
    :returns: List of hpccm objects that can be attached to the stage
    :rtype: List

    """

    commands = [
        comment("#######################"),
        comment("###### gcc layer ######"),
        comment("#######################"),
    ]

    versions = get_versions("gcc", ubuntu_version, versions)

    for version in versions:
        commands.append(
            gnu(
                version=version,
                extra_repository=True,  # install from ppa and not source
            )
        )

        add_installed_software("gcc", version, "/usr")

    return commands
