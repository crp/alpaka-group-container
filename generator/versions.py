# Copyright 2023 Simeon Ehrig
#
# ISC Software License

"""Handling software versions.
"""

from typing import Dict, List

ubuntu_versions: Dict[float, Dict[str, str]] = {
    22.04: {"image": "ubuntu:jammy", "distro": "ubuntu22"},
    24.04: {"image": "ubuntu:noble", "distro": "ubuntu24"},
}

sw_versions: Dict[str, List[str]] = {
    "gcc": ["10", "11", "12", "13", "14"],
    "boost": [
        "1.74.0",
        "1.75.0",
        "1.76.0",
        "1.77.0",
        "1.78.0",
        "1.79.0",
        "1.80.0",
        "1.81.0",
        "1.82.0",
        "1.83.0",
        "1.84.0",
        "1.85.0",
        "1.86.0",
        "1.87.0",
    ],
    "catch2": [],
    "cmake": [
        "3.22.6",
        "3.23.5",
        "3.24.4",
        "3.25.3",
        "3.26.6",
        "3.27.9",
        "3.28.6",
        "3.29.9",
        "3.30.6",
        "3.31.4",
    ],
    "cuda": [
        "12.0",
        "12.1",
        "12.2",
        "12.3",
        "12.4",
        "12.5",
        "12.6",
    ],
    "rocm": ["6.0", "6.1", "6.2", "6.3"],
}


def get_versions(
    sw_name: str, ubuntu_version: float, input_list: List[str] = []
) -> List[str]:
    """Ether returns list of supported versions of specific software for a certain Ubuntu version
    or remove unsupported software version from a list.

    Args:
        sw_name (str): Name of the software, e.g. cmake, gcc, cuda ect.
        ubuntu_version (float): Ubuntu version.
        input_list (List[str], optional): List to filter. If list is empty, return list of all
        supported software versions for the specified Ubuntu version. Defaults to [].

    Raises:
        RuntimeError: If the ubuntu version is unknown.
        RuntimeError: If the software name is unknown.

    Returns:
        List[str]: List of supported versions.
    """
    if not ubuntu_version in ubuntu_versions:
        raise RuntimeError(
            f"unknown ubuntu version: {ubuntu_version}\nKnow versions: {ubuntu_versions}"
        )

    if not sw_name in sw_versions.keys():
        raise RuntimeError(
            f"unknown software name: {sw_name}\nKnow versions: {sw_versions.keys()}"
        )

    if not input_list:
        input_list = sw_versions[sw_name]

    return input_list
