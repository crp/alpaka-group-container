# Copyright 2023 Simeon Ehrig
#
# ISC Software License

"""Install cmake.
"""

from typing import List
from hpccm.primitives import comment, shell
from hpccm.building_blocks.boost import boost

from generator.util import add_installed_software, CMakeBuildPath
from generator.versions import get_versions


def boost_layer(
    versions: List[str],
    ubuntu_version: float,
    install_prefix: str = "/opt/boost",
    toolset_compiler: str = "gcc",
) -> List:
    """Install boost from source.

    :param versions: List of the version to be installed.
    :type versions: List[str]
    :param ubuntu_version: Ubuntu version of used base container (eg. 20.04).
    :type ubuntu_version: float
    :param install_prefix: Boost is installed to <install_prefix>/<version>.
    :type install_prefix: str
    :param toolset_compiler: Set boost bootstrap option "--tool-set". Supported are gcc and clang.
    :type toolset_compiler: str
    :returns: List of hpccm objects that can be attached to the stage
    :rtype:

    """

    if toolset_compiler != "gcc" and toolset_compiler != "clang":
        raise ValueError("unknown boost toolset compiler: " + toolset_compiler)

    versions = get_versions("boost", ubuntu_version, versions)

    commands = [
        comment("#######################"),
        comment("##### boost layer #####"),
        comment("#######################"),
    ]

    with CMakeBuildPath(commands):
        commands.append(shell(commands=["mkdir -p /var/tmp", "mkdir -p /var/tmp/logs"]))

        for version in versions:
            boost_install = boost(
                baseurl="https://archives.boost.io/release/__version__/source",
                prefix=install_prefix + "/" + version,
                version=version,
                environment=False,
                bootstrap_opts=["--with-libraries=atomic,program_options"],
                b2_opts=['cxxflags="-std=c++20"'],
            )

            # write build output of ./b2 to log file
            boost_str = ""
            for line in str(boost_install).split("\n"):
                if "./b2" in line:
                    idx = line.index("&& \\")
                    line = (
                        line[:idx]
                        + f" 2>&1 > /var/tmp/logs/boost_{version}.log "
                        + line[idx:]
                    )
                boost_str += line + "\n"

            commands.append(shell(commands=[boost_str]))

            add_installed_software("boost", version, install_prefix + "/" + version)

        commands.append(shell(commands=["rm -r /var/tmp/logs/boost_*"]))

    return commands
