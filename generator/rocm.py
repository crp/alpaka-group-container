# Copyright 2023 Simeon Ehrig
#
# ISC Software License

"""Install ROCm SDK.
"""

from typing import Dict, List

from hpccm.primitives import comment, shell, environment
from hpccm.building_blocks.packages import packages
from generator.util import add_installed_software
from generator.versions import sw_versions, get_versions, ubuntu_versions


# rocm base images
rocm_versions: Dict[float, Dict[str, str]] = {
    6.0: {},
    6.1: {},
    6.2: {},
    6.3: {},
}

if not set(sw_versions["rocm"]) == set([str(v) for v in rocm_versions.keys()]):
    raise RuntimeError(
        'sw_versions["rocm"]) and rocm_versions.keys() does not contain the same ROCm versions.'
    )


def rocm_layer(version: float, ubuntu_version: float) -> List:
    """Provide commands to install different ROCm SDK versions.

    :param version: ROCm SDK version
    :type: float
    :param ubuntu_version: Ubuntu version of used base container (eg. 20.04).
    :type ubuntu_version: float
    :returns: List of hpccm objects that can be attached to the stage
    :rtype: List

    """
    if not str(version) in sw_versions["rocm"]:
        raise RuntimeError(
            f"unknown ROCm version: {version}\nKnow ROCm versions: {sw_versions['cuda']}"
        )

    # use get_versions function for verification
    # if list is empty, the combination of CUDA and Ubuntu version is unsupported
    if not get_versions("rocm", ubuntu_version, [str(version)]):
        raise RuntimeError(
            f"unsupported combination of ROCm SDK {version} and Ubuntu {ubuntu_version}"
        )

    commands: List = [
        comment("#######################"),
        comment("##### ROCm layer ######"),
        comment("#######################"),
    ]

    ubuntu_code_name = ubuntu_versions[ubuntu_version]["image"].split(":")[1]

    commands.append(packages(ospackages=["gnupg2", "libnuma-dev"]))

    ppa_key = (
        "wget https://repo.radeon.com/rocm/rocm.gpg.key -O - | "
        " gpg --dearmor | "
        "tee /etc/apt/keyrings/rocm.gpg >/dev/null"
    )
    repostory_url = (
        'echo "deb [arch=amd64 signed-by=/etc/apt/keyrings/rocm.gpg] '
        f'https://repo.radeon.com/rocm/apt/{version} {ubuntu_code_name} main " | '
        "tee --append /etc/apt/sources.list.d/rocm.list"
    )

    commands.append(shell(commands=[ppa_key, repostory_url]))

    commands.append(
        packages(
            ospackages=[
                "libelf1",
                "kmod",
                "file",
                "python3",
                f"rocm-dev{version}",
                "build-essential",
                f"rocrand-dev{version}",
                f"hiprand-dev{version}",
            ]
        )
    )

    # The path is necessary, that cmake finds the hip target.
    commands.append(
        environment(
            variables={
                "PATH": "/opt/rocm/bin:$PATH",
            }
        )
    )

    add_installed_software("rocm", str(version), "/opt/rocm")

    return commands
