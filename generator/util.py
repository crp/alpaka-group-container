# Copyright 2023 Simeon Ehrig
#
# ISC Software License

"""Provide different custom layers.
"""


import json
from typing import Dict, List
from distutils.version import StrictVersion

import hpccm
from hpccm.primitives import shell, comment, environment
from hpccm.building_blocks.packages import packages

# contains information about installed software, like version number and base folder
# is dumped as .json file in /etc/installed_software.json in the container
installed_software: Dict = {}


def add_installed_software(name: str, version: str, base_path: str):
    """Add software entry to installed_software.

    :param name: name of the software
    :type name: str
    :param version: version of the software
    :type version: str
    :param base_path: Base path of the installed software. If there is no path, keep it empty.
    :type base_path: str
    """
    # use software name and version as unique key
    name_version_str = str(name) + "@" + str(version)
    installed_software[name_version_str] = {
        "base_path": base_path,
    }


def copy_agc_manager() -> List:
    """Copy agc-manager.py in the container and create start script

    :returns: list of hpccm primitives
    :rtype: List[hpccm.primitives]
    """
    steps = [hpccm.primitives.copy(src="tools/agc-manager.py", dest="/usr/bin")]
    steps.append(
        shell(
            commands=[
                "printf '#!/bin/bash\\n\\n/usr/bin/python3 /usr/bin/agc-manager.py $@'"
                " > /usr/bin/agc-manager",
                "chmod +x /usr/bin/agc-manager",
            ]
        )
    )

    return steps


def base_layer(ubuntu_version: float) -> List:
    """Add a base layer, which installs some package and tools
    for following up layers.

    :returns: list of hpccm primitives
    :rtype: List[hpccm.primitives]
    """
    commands: List = [
        comment("#######################"),
        comment("##### base layer ######"),
        comment("#######################"),
    ]

    apt_package_list = [
        "bzip2",
        "libbz2-dev",
        "gcc",
        "g++",
        "git",
        "libc6-dev",
        "libomp-dev",
        "make",
        "rsync",
        "software-properties-common",
        "tar",
        "wget",
        "zlib1g-dev",
        "gpg-agent",
    ]

    commands.append(packages(ospackages=apt_package_list))

    commands += copy_agc_manager()

    if ubuntu_version == 20.04:
        add_installed_software("gcc", 9, "/usr")

    return commands


def final_layer() -> List:
    """Add a final layer, which is required for all recipe variants

    :returns: list of hpccm primitives
    :rtype: List[hpccm.primitives]
    """

    commands: List = [
        comment("#######################"),
        comment("##### final layer #####"),
        comment("#######################"),
    ]

    commands.append(hpccm.primitives.copy(src="tools/json_merge.py", dest="/opt"))
    commands.append(
        # if /etc/installed_software.json does not exist write installed software in
        # else extend /etc/installed_software.json with new installed software
        shell(
            commands=[
                f"""echo '{json.dumps(installed_software)}' > /opt/tmp.json && \\
if [ ! -f "/etc/installed_software.json" ]; then \\
  mv /opt/tmp.json /etc/installed_software.json ; \\
else \\
  cat /etc/installed_software.json && \\
  python3 /opt/json_merge.py -o /etc/installed_software.json -n /opt/tmp.json > /opt/tmp2.json && \\
  mv /opt/tmp2.json /etc/installed_software.json && \\
  rm /opt/tmp.json; \\
fi && \\
rm /opt/json_merge.py
"""
            ]
        ),
    )
    return commands


class CMakeBuildPath:
    """Context class, which adds temporary a cmake installation to the path
    and remove the path after leaving the context.
    """

    def __init__(self, commands: list):
        """Add hpccm commands to the commands list, if the context area is
        entered and if it is leaved

        Args:
            commands (list): list of hpccm commands
        """
        self.commands = commands

    def __enter__(self):
        self.commands.append(
            environment(
                variables={
                    "CONTAINER_BUILD_TMP_PATH": "$PATH",
                }
            ),
        )
        self.commands.append(
            environment(
                variables={
                    "PATH": "$CONTAINER_BUILD_CMAKE_BIN:$PATH",
                }
            ),
        )

    def __exit__(self, *args):
        self.commands.append(
            environment(
                variables={
                    "PATH": "$CONTAINER_BUILD_TMP_PATH",
                }
            ),
        )
        self.commands.append(
            environment(
                variables={
                    "CONTAINER_BUILD_TMP_PATH": "",
                }
            ),
        )
