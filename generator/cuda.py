# Copyright 2023 Simeon Ehrig
#
# ISC Software License

"""Install CUDA SDK.
"""

from typing import Dict, List

from hpccm.primitives import comment, shell, environment
from hpccm.building_blocks.packages import packages
from generator.util import add_installed_software
from generator.versions import sw_versions, get_versions

cuda_versions: Dict[float, Dict[str, str]] = {
    12.0: {
        "deb-name": "cuda-repo-ubuntu2204-12-0-local",
        "file-name": "cuda-repo-ubuntu2204-12-0-local_12.0.0-525.60.13-1_amd64.deb",
        "url": "https://developer.download.nvidia.com/compute/cuda/12.0.0/local_installers/",
    },
    12.1: {
        "deb-name": "cuda-repo-ubuntu2204-12-1-local",
        "file-name": "cuda-repo-ubuntu2204-12-1-local_12.1.0-530.30.02-1_amd64.deb",
        "url": "https://developer.download.nvidia.com/compute/cuda/12.1.0/local_installers/",
    },
    12.2: {
        "deb-name": "cuda-repo-ubuntu2204-12-2-local",
        "file-name": "cuda-repo-ubuntu2204-12-2-local_12.2.0-535.54.03-1_amd64.deb",
        "url": "https://developer.download.nvidia.com/compute/cuda/12.2.0/local_installers/",
    },
    12.3: {
        "deb-name": "cuda-repo-ubuntu2204-12-3-local",
        "file-name": "cuda-repo-ubuntu2204-12-3-local_12.3.0-545.23.06-1_amd64.deb",
        "url": "https://developer.download.nvidia.com/compute/cuda/12.3.0/local_installers/",
    },
    12.4: {
        "deb-name": "cuda-repo-ubuntu2204-12-4-local",
        "file-name": "cuda-repo-ubuntu2204-12-4-local_12.4.0-550.54.14-1_amd64.deb",
        "url": "https://developer.download.nvidia.com/compute/cuda/12.4.0/local_installers/",
    },
    12.5: {
        "deb-name": "cuda-repo-ubuntu2204-12-5-local",
        "file-name": "cuda-repo-ubuntu2204-12-5-local_12.5.0-555.42.02-1_amd64.deb",
        "url": "https://developer.download.nvidia.com/compute/cuda/12.5.0/local_installers/",
    },
    12.6: {
        "deb-name": "cuda-repo-ubuntu2404-12-6-local",
        "file-name": "cuda-repo-ubuntu2404-12-6-local_12.6.3-560.35.05-1_amd64.deb",
        "url": "https://developer.download.nvidia.com/compute/cuda/12.6.3/local_installers/",
    },
}

if not set(sw_versions["cuda"]) == set([str(v) for v in cuda_versions.keys()]):
    raise RuntimeError(
        'sw_versions["cuda"]) and cuda_versions.keys() does not contain the same cuda versions.'
    )


def cuda_layer(version: float, ubuntu_version: float) -> List:
    """Provide commands to install different CUDA SDK versions.

    :param version: CUDA SDK version
    :type: float
    :param ubuntu_version: Ubuntu version of used base container (eg. 20.04).
    :type ubuntu_version: float
    :returns: List of hpccm objects that can be attached to the stage
    :rtype: List

    """
    if not str(version) in sw_versions["cuda"]:
        raise RuntimeError(
            f"unknown CUDA version: {version}\nKnow CUDA versions: {sw_versions['cuda']}"
        )

    # use get_versions function for verification
    # if list is empty, the combination of CUDA and Ubuntu version is unsupported
    if not get_versions("cuda", ubuntu_version, [str(version)]):
        raise RuntimeError(
            f"unsupported combination of CUDA SDK {version} and Ubuntu {ubuntu_version}"
        )

    # e.g. `11.1`` -> 11-1``
    version_dash = str(version).replace(".", "-")

    commands: List = [
        comment("#######################"),
        comment("##### CUDA layer ######"),
        comment("#######################"),
    ]

    apt_package_list = ["dirmngr", "wget", "gpg-agent", "g++-multilib", "gnupg2"]

    commands.append(packages(ospackages=apt_package_list))

    shell_commands = []
    shell_commands.append(
        "apt-key adv --keyserver keyserver.ubuntu.com --recv-keys F60F4B3D7FA2AF80"
    )
    shell_commands.append('echo "download CUDA SDK"')

    # download .deb package from nvidia server
    shell_commands.append(
        "wget --no-verbose -O "
        + cuda_versions[version]["file-name"]
        + " "
        + cuda_versions[version]["url"]
        + cuda_versions[version]["file-name"]
    )
    shell_commands.append('echo "finish downloading CUDA SDK"')
    shell_commands.append("dpkg -i ./" + cuda_versions[version]["file-name"])

    # Since 11.7 the keys are shipped as part of the local repository.
    if version > 11.6:
        shell_commands.append(
            f'cp /var/{cuda_versions[version]["deb-name"]}/cuda-*-keyring.gpg /usr/share/keyrings'
        )

    shell_commands.append("apt -y --quiet update")

    cuda_apt_packages = (
        f"cuda-compiler-{version_dash} "
        f"cuda-cudart-{version_dash} "
        f"cuda-cudart-dev-{version_dash} "
        f"libcurand-{version_dash} "
        f"libcurand-dev-{version_dash}"
    )

    shell_commands.append(
        (
            "DEBIAN_FRONTEND=noninteractive apt -y --quiet "
            "--allow-unauthenticated --no-install-recommends install "
            + cuda_apt_packages
        )
    )
    # the CUDA 11.3 apt packages creates the link at installation
    # older SDKs not
    shell_commands.append(
        (
            'if [ ! -d "/usr/local/cuda" ]; then '
            f"ln -s /usr/local/cuda-{version} /usr/local/cuda; "
            "fi"
        )
    )
    shell_commands.append("rm -rf " + cuda_versions[version]["file-name"])
    # The downloaded .deb package is a kind of archive that contains all the CUDA SDK software.
    # When a cuda package is installed, it is a copy operation from that .deb package.
    # Therefore, it can be removed after installing all the required Cuda packages.
    #   -> This shrinks the container size by quite a bit.
    #
    # I have not found a template rule for the full installation name of the .deb package.
    # I only know how the name starts. Therefore I have to search for the full name.
    shell_commands.append(
        (
            "dpkg --purge "
            "$(dpkg --get-selections "
            f'| grep {cuda_versions[version]["deb-name"]} '
            "|  awk '{print $1;}')"
        )
    )
    shell_commands.append("rm -rf /var/lib/apt/lists/*")

    commands.append(shell(commands=shell_commands))

    # The path is necessary, that cmake finds the CUDA driver API.
    commands.append(
        environment(
            variables={
                "PATH": "/usr/local/cuda/bin:$PATH",
                "LIBRARY_PATH": "/usr/local/cuda/lib64/stubs:$LIBRARY_PATH",
            }
        )
    )

    add_installed_software("cuda", str(version), "/usr/local/cuda")

    return commands
