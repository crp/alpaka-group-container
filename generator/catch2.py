# Copyright 2023 Simeon Ehrig
#
# ISC Software License

"""Install Catch2 test framework
"""

from typing import List
import os

from hpccm.primitives import comment, shell
from hpccm.templates.git import git
from hpccm.templates.CMakeBuild import CMakeBuild
from hpccm.templates.rm import rm

from generator.util import add_installed_software, CMakeBuildPath
from generator.versions import get_versions


def catch2_layer(
    versions: List[str], ubuntu_version: float, install_prefix: str = "/opt/catch2"
) -> List:
    """Provide commands to install different Catch2 versions.

    :param versions: List of the version to be installed.
    :type versions: List[str]
    :param ubuntu_version: Ubuntu version of used base container (eg. 20.04).
    :type ubuntu_version: float
    :param install_prefix: Catch2 is installed to <install_prefix>/<version>.
    :type install_prefix: str
    :returns: list of hpccm primitives
    :rtype: List[hpccm.primitives]
    """

    versions = get_versions("catch2", ubuntu_version, versions)

    commands = [
        comment("#######################"),
        comment("##### catch2 layer ####"),
        comment("#######################"),
    ]

    with CMakeBuildPath(commands):
        for version in versions:
            install_path = os.path.join(install_prefix, version)
            shell_commands: List = []

            catch_git = git()
            shell_commands.append(
                catch_git.clone_step(
                    repository="https://github.com/catchorg/Catch2.git",
                    branch=version,
                    path="/opt/repos",
                )
            )

            catch_cmake = CMakeBuild(prefix=install_path)
            shell_commands.append(
                catch_cmake.configure_step(
                    directory="/opt/repos/Catch2",
                )
            )
            shell_commands.append(catch_cmake.build_step(target="install"))

            add_installed_software("catch2", version, install_path)

            # remove repos and build folder
            rm_instance = rm()
            shell_commands.append(rm_instance.cleanup_step(items=["/opt/repos/"]))

            commands.append(shell(commands=shell_commands))

    return commands
