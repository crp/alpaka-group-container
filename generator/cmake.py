# Copyright 2023 Simeon Ehrig
#
# ISC Software License

"""Install cmake.
"""

from typing import List
from hpccm.primitives import comment, environment
from hpccm.building_blocks.cmake import cmake
from generator.util import add_installed_software
from generator.versions import get_versions


def cmake_layer(
    versions: List[str], ubuntu_version: float, install_prefix: str = "/opt/cmake"
) -> List:
    """Provide commands to install different cmake versions.

    :param versions: List of the version to be installed.
    :type versions: List[str]
    :param ubuntu_version: Ubuntu version of used base container (eg. 20.04).
    :type ubuntu_version: float
    :param install_prefix: CMake is installed to <install_prefix>/<version>.
    :type install_prefix: str
    :returns: list of hpccm primitives
    :rtype: List[hpccm.primitives]
    """

    versions = get_versions("cmake", ubuntu_version, versions)

    commands: List = [
        comment("#######################"),
        comment("##### cmake layer #####"),
        comment("#######################"),
    ]

    for version in versions:
        prefix_path = install_prefix + "/" + version
        cmake_commands = cmake(eula=True, version=version, prefix=prefix_path)

        # by default, hpccm add each cmake installation to the PATH variable
        # we need to manually remove this
        cmake_commands_filters = []
        for cm in cmake_commands:
            if not (isinstance(cm, environment) and "PATH=" in str(cm)):
                cmake_commands_filters.append(cm)

        commands.append(cmake_commands_filters)
        add_installed_software("cmake", version, prefix_path)

    commands.append(
        environment(
            variables={
                "CONTAINER_BUILD_CMAKE_BIN": f"{install_prefix}/{versions[-1]}/bin",
            }
        )
    )

    return commands
