# Copyright 2022 Simeon Ehrig
#
# ISC Software License

"""Install PIConGPU dependencies.
"""

from typing import List, Dict

from hpccm.primitives import comment, shell
from hpccm.building_blocks.packages import packages
from hpccm.templates.git import git
from hpccm.templates.CMakeBuild import CMakeBuild
from hpccm.templates.ConfigureMake import ConfigureMake
from hpccm.templates.rm import rm
from hpccm.templates.downloader import downloader

from generator.util import add_installed_software, CMakeBuildPath


class EnvHelper:
    """Small helper class that creates 'export' bash commands for each stage (the export command in
    a Docker RUN command is only valid within the RUN command)
    """

    def __init__(self):
        """Setup an instance with empty dictionaries"""
        self.single_env: Dict[str, str] = {}
        self.env_vars: Dict[str, List[str]] = {}

    def add(self, name: str, val: str):
        """Set export name=val. Overwrite existing variables

        :param name: name of the variable
        :type name: str
        :param val: value of the variable
        :type val: str

        """
        self.single_env[name] = val

    def append(self, name: str, val: str):
        """Set export name=val:$name . If a variable already exists, append the variable.

        :param name: name of the variable
        :type name: str
        :param val: value of the variable
        :type val: str

        """
        if name in self.env_vars:
            self.env_vars[name].insert(0, val)
        else:
            self.env_vars[name] = [val]

    def get_export_cmd(self) -> List[str]:
        """Returns all bash 'export' commands.

        :returns: list of bash 'export' commands
        :rtype: List[str]

        """
        cmds: List[str] = []
        for env_name, env_value in self.single_env.items():
            cmds.append(f"export {env_name}={env_value}")

        for env_name, env_values in self.env_vars.items():
            export_cmd = f"export {env_name}="
            for env_value in env_values:
                export_cmd += env_value + ":"
            export_cmd += "$" + env_name
            cmds.append(export_cmd)
        return cmds


def picongpu_layer() -> List:
    """Build all dependencies of PIConGPU.

    :returns: List of hpccm objects that can be attached to the stage
    :rtype: List

    """

    env_helper = EnvHelper()

    picongpu_install_instr = [
        comment("#######################"),
        comment("#### picongpu layer ###"),
        comment("#######################"),
    ]

    picongpu_install_instr.append(
        packages(
            ospackages=[
                # for pngwriter
                "libpng-dev",
                # mpi general
                "libopenmpi-dev",
                "openmpi-bin",
                "openssh-server",
                # for HDF5
                "zlib1g-dev",
            ]
        )
    )

    with CMakeBuildPath(picongpu_install_instr):

        #######################
        ###### pngwriter ######
        #######################
        picongpu_install_instr.append(comment("## install pngwriter ##"))

        commands = []

        pngwriter_git = git()
        commands.append(
            pngwriter_git.clone_step(
                repository="https://github.com/pngwriter/pngwriter.git",
                branch="0.7.0",
                path="/opt/repos",
            )
        )

        pngwriter_cmake = CMakeBuild(prefix="/opt/pngwriter/0.7.0")
        commands.append(
            pngwriter_cmake.configure_step(directory="/opt/repos/pngwriter")
        )
        commands.append(pngwriter_cmake.build_step(target="install"))

        env_helper.append(name="CMAKE_PREFIX_PATH", val="/opt/pngwriter/0.7.0")

        add_installed_software("pngwriter", "0.7.0", "/opt/pngwriter/0.7.0")
        picongpu_install_instr.append(shell(commands=commands))

        #######################
        ######## HDF5 #########
        #######################
        picongpu_install_instr.append(comment("## install HDF5 ##"))

        commands = env_helper.get_export_cmd()

        hdf5_downloader = downloader(
            url="https://support.hdfgroup.org/releases/hdf5/v1_14/v1_14_5/downloads/hdf5-1.14.5.tar.gz"
        )

        commands.append(hdf5_downloader.download_step(unpack=True, wd="/opt/repos"))

        hdf5_configure_make = ConfigureMake(prefix="/opt/hdf5/1.14.5")
        commands.append(
            hdf5_configure_make.configure_step(
                directory="/opt/repos/hdf5-1.14.5",
                build_directory="/opt/repos/hdf5-1.14.5/build",
                opts=[
                    "--enable-parallel",
                    "--enable-shared",
                ],
            )
        )
        commands.append(hdf5_configure_make.build_step())
        commands.append(hdf5_configure_make.install_step())

        env_helper.add(name="HDF5_ROOT", val="/opt/hdf5/1.14.5")
        env_helper.append(name="LD_LIBRARY_PATH", val="/opt/hdf5/1.14.5/lib")

        add_installed_software("hdf5", "1.14.5", "/opt/hdf5/1.14.5")
        picongpu_install_instr.append(shell(commands=commands))

        #######################
        ####### ADIOS 2 #######
        #######################
        picongpu_install_instr.append(comment("## install ADIOS 2 ##"))

        commands = env_helper.get_export_cmd()
        commands += ["export CC=mpicc", "export CXX=mpicxx"]

        adios2_git = git()
        commands.append(
            adios2_git.clone_step(
                repository="https://github.com/ornladios/ADIOS2",
                branch="v2.10.2",
                path="/opt/repos",
            )
        )

        adios2_cmake = CMakeBuild(prefix="/opt/adios/2.10.2")
        commands.append(
            adios2_cmake.configure_step(
                directory="/opt/repos/ADIOS2",
                opts=[
                    "-DADIOS2_BUILD_EXAMPLES=OFF",
                    "-DADIOS2_BUILD_TESTING=OFF",
                    "-DADIOS2_USE_Fortran=OFF",
                    "-DCMAKE_CXX_FLAGS=-fPIC",
                    "-DCMAKE_C_FLAGS=-fPIC",
                ],
            )
        )
        commands.append(adios2_cmake.build_step(target="install"))

        env_helper.append(name="CMAKE_PREFIX_PATH", val="/opt/adios/2.10.2")
        env_helper.append(name="PATH", val="/opt/adios/2.10.2/bin")
        commands += ["unset CC", "unset CXX"]

        add_installed_software("adios2", "2.10.2", "/opt/adios/2.10.2")
        picongpu_install_instr.append(shell(commands=commands))

        #######################
        ####### jansson #######
        #######################
        picongpu_install_instr.append(comment("## install jansson ##"))

        commands = env_helper.get_export_cmd()

        jansson_git = git()
        commands.append(
            jansson_git.clone_step(
                repository="https://github.com/akheron/jansson.git",
                branch="v2.9",
                path="/opt/repos",
            )
        )

        jansson_cmake = CMakeBuild(prefix="/opt/jansson/2.9.0")
        commands.append(
            jansson_cmake.configure_step(
                directory="/opt/repos/jansson",
            )
        )
        commands.append(jansson_cmake.build_step(target="install"))

        env_helper.append(name="CMAKE_PREFIX_PATH", val="/opt/jansson/2.9.0/lib/cmake")

        add_installed_software("jansson", "2.9.0", "/opt/jansson/2.9.0")
        picongpu_install_instr.append(shell(commands=commands))

        #######################
        ######## icet #########
        #######################
        picongpu_install_instr.append(comment("## install icet ##"))

        commands = env_helper.get_export_cmd()

        icet_git = git()
        commands.append(
            icet_git.clone_step(
                repository="https://gitlab.kitware.com/paraview/icet.git",
                branch="IceT-2.1.1",
                path="/opt/repos",
            )
        )

        icet_cmake = CMakeBuild(prefix="/opt/icet/2.9.0")
        commands.append(
            icet_cmake.configure_step(
                directory="/opt/repos/icet",
                opts=["-DICET_USE_OPENGL=OFF", "-DBUILD_TESTING=OFF"],
            )
        )
        commands.append(icet_cmake.build_step(target="install"))

        env_helper.append(name="CMAKE_PREFIX_PATH", val="/opt/icet/2.9.0")

        add_installed_software("icet", "2.9.0", "/opt/icet/2.9.0")
        picongpu_install_instr.append(shell(commands=commands))

        #######################
        ##### openpmd-api #####
        #######################
        picongpu_install_instr.append(comment("## install openpmd-api ADIOS 2 ##"))

        commands = env_helper.get_export_cmd()

        openpmd_git = git()
        commands.append(
            openpmd_git.clone_step(
                repository="https://github.com/openPMD/openPMD-api.git",
                commit="0.16.1",
                path="/opt/repos",
            )
        )

        openpmd_cmake = CMakeBuild(prefix="/opt/openPMD-api/0.16.1")
        commands.append(
            openpmd_cmake.configure_step(
                directory="/opt/repos/openPMD-api",
                opts=[
                    "-DopenPMD_USE_ADIOS2=ON",
                    "-DopenPMD_USE_ADIOS1=OFF",
                    "-DopenPMD_USE_PYTHON=OFF",
                ],
            )
        )
        commands.append(openpmd_cmake.build_step(target="install"))

        env_helper.append(name="CMAKE_PREFIX_PATH", val="/opt/openPMD-api/0.16.1")

        add_installed_software("openPMD-api", "0.16.1", "/opt/openPMD-api/0.16.1")
        picongpu_install_instr.append(shell(commands=commands))

    #######################
    ###### clean up #######
    #######################

    # remove repos and build folder
    rm_instance = rm()
    picongpu_install_instr.append(
        shell(commands=[rm_instance.cleanup_step(items=["/opt/repos/"])])
    )

    return picongpu_install_instr
