# Copyright 2023 Simeon Ehrig
#
# ISC Software License

"""Provide different layer for container recipe generation
"""

import sys
from distutils.version import StrictVersion

import hpccm

from generator.util import (
    base_layer,
    final_layer,
    add_installed_software,
)
from generator.versions import ubuntu_versions, sw_versions
from generator.cmake import cmake_layer
from generator.boost import boost_layer
from generator.catch2 import catch2_layer
from generator.gcc import gcc_layer
from generator.clang import clang_ppa_layer
from generator.cuda import cuda_layer, cuda_versions
from generator.rocm import rocm_layer, rocm_versions
from generator.picongpu import picongpu_layer

required_hpccm_version: str = "21.10.0"
if StrictVersion(hpccm.__version__) < StrictVersion(required_hpccm_version):
    print(
        "hpccm version is to old: "
        + hpccm.__version__
        + "\nPlease install at least version: "
        + required_hpccm_version,
        file=sys.stderr,
    )
    exit(1)
